
import {
  Component, ElementRef, forwardRef, HostListener, Input, OnInit, ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'n-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DropdownComponent),
      multi: true
    }
  ]
})
export class DropdownComponent
  implements OnInit, ControlValueAccessor {

  constructor(private _eref: ElementRef) { }
  @Input() dropdownOptions: { label: string, value: any }[];
  @Input() label = 'Label';
  @Input() validationError = false;
  @Input() disabled = false;
  @Input() errorLabel: string;


  public selectedOption: any;
  highlightedOptionIndex = 0;
  showDropdown = false;

  @ViewChild('dropdownSurface', { static: false }) dropdownSurfaceViewChild: ElementRef;

  ngOnInit() { }

  @HostListener('document:keydown', ['$event'])
  onKeydown(event: any) {

    if (event.keyCode === 9 && this.showDropdown) {
      this.closeDropdown();
    }

    if (event.keyCode === 40 && this.showDropdown) { // if down arrow pressed
      event.preventDefault();
      if (this.highlightedOptionIndex === this.dropdownOptions.length - 1) {
        this.highlightedOptionIndex = 0;
        document.querySelector('.highlighted')
          .parentElement.parentElement.firstElementChild
          .firstElementChild.scrollIntoView({
            block: 'nearest',
            inline: 'nearest'
          });

      } else {
        this.highlightedOptionIndex++;
        document.querySelector('.highlighted')
          .parentElement.nextElementSibling
          .firstElementChild.scrollIntoView({
            block: 'nearest',
            inline: 'nearest'
          });
      }
    }

    if (event.keyCode === 38 && this.showDropdown) { // if up arrow pressed
      event.preventDefault();

      if (this.highlightedOptionIndex === 0) {
        this.highlightedOptionIndex = this.dropdownOptions.length - 1; // reset highlighted state to bottom if scroll up on zero
        document.querySelector('.highlighted')
          .parentElement.parentElement.lastElementChild
          .firstElementChild.scrollIntoView({
            block: 'nearest',
            inline: 'nearest'
          });

      } else {
        this.highlightedOptionIndex--;
        document.querySelector('.highlighted')
          .parentElement.previousElementSibling
          .firstElementChild.scrollIntoView({
            block: 'nearest',
            inline: 'nearest'
          });
      }
    }

    if (event.keyCode === 13) {
      if (this.showDropdown) { // if enter key pressed
        const highlightedOption = this.dropdownOptions[this.highlightedOptionIndex];
        this.selectOption(highlightedOption);
      } else if (document.activeElement === this._eref.nativeElement) {
        if (!this.disabled) {
          this.showDropdown = true;
        }
      }
    }
  }

  @HostListener('document:mousedown', ['$event'])
  onClick(event: any) {
    if (!this._eref.nativeElement.contains(event.target)) {
      this.showDropdown = false;
    }
  }

  checkHighlighted(index: number) {
    return index === this.highlightedOptionIndex;
  }

  setHighlightedOption(index: number) {
    this.highlightedOptionIndex = index;
  }

  writeValue(value: any) {
    try {
      if (value !== this.selectedOption) {
        this.selectedOption = value;
      }
    } catch (e) { }
  }

  registerOnTouched() { }

  propagateChange = (_: any) => { };

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  selectOption(option: any) {
    this._eref.nativeElement.blur();
    this.selectedOption = option;
    this.propagateChange(this.selectedOption);
    this.closeDropdown();
  }

  toggleDropdown() {
    if (!this.disabled) {
      this.showDropdown = !this.showDropdown;
      if (this.showDropdown) {
        this.highlightedOptionIndex = 0;
      }
    }

  }
  openDropdown() {
    if (!this.disabled) {
      this.highlightedOptionIndex = 0;
      this.showDropdown = true;
    }
  }
  closeDropdown() {
    this.showDropdown = false;
    this._eref.nativeElement.focus();
  }
}
