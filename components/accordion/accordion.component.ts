import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, ElementRef, HostListener, OnInit, ViewChild, Input } from '@angular/core';

let count = 1;

@Component({
  selector: 'n-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss'],
  animations: [
    trigger('tabContent', [
      state('hidden', style({
        height: '70px'
      })),
      state('out', style({
        height: '*'
      })),
      transition('visible <=> hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
    ])
  ],
})
export class AccordionComponent implements OnInit {

  accordionVisible = 'hidden';
  innerContentVisible: boolean;
  instanceCount: number = ++count;

  @Input() tabIndex: number;

  @ViewChild('header', { static: true }) header: ElementRef<HTMLElement>;

  constructor() { }

  ngOnInit() {
    this.accordionVisible = 'hidden';
  }

  @HostListener('keydown', ['$event'])
  onKeydown(event: any) {
    if (event.keyCode === 13 && document.activeElement.classList.contains('p-accordion')) {
      if (this.accordionVisible === 'hidden') {
        this.accordionVisible = 'visible';
        this.header.nativeElement.classList.add('active');
      } else {
        this.accordionVisible = 'hidden';
        this.header.nativeElement.classList.remove('active');
      }
    }
  }

  toggleInnerContent(event: any) {
    if (event.phaseName === 'start') {
      if (event.toState === 'visible') {
        // timeout ensures no console error on double click
        setTimeout(() => {
          this.innerContentVisible = true;
        });
      }
    } else {
      if (event.toState === 'hidden') {
        // timeout ensures no console error on double click
        setTimeout(() => {
          this.innerContentVisible = false;
        });
      }
    }
  }

  toggleAccordion(event: Event) {
    this.accordionVisible = this.accordionVisible === 'hidden' ? 'visible' : 'hidden';
    if (this.accordionVisible === 'visible') {
      this.header.nativeElement.classList.add('active');
    } else {
      this.header.nativeElement.classList.remove('active');
    }
  }

  collapseAccordion() {
    this.accordionVisible = 'hidden';
    this.header.nativeElement.classList.remove('active');
  }
}

